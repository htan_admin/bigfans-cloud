package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * 
 * @Description:商品属性,和分类是一对多的关系.
 * @author lichong 
 * 2015年8月9日下午7:36:28
 *
 */
@Data
@Table(name="AttributeOption")
public class AttributeOptionEntity extends AbstractModel {

	private static final long serialVersionUID = 8949896457611748295L;
	
	public static final String INPUTTYPE_LIST = "L";
	public static final String INPUTTYPE_MANUL = "M";

	@Column(name="category_id")
	protected String categoryId;
	@Column(name="name")
	protected String name;
	@OrderBy
	@Column(name="order_num")
	protected Integer orderNum;
	// 输入类型, L 从列表选择 , M手动输入
	@Column(name="input_type")
	protected String inputType;
	@Column(name="searchable")
	protected Boolean searchable;
	@Column(name="required")
	protected Boolean required;

	@Override
	public String getModule() {
		return "AttributeOption";
	}
	
}
