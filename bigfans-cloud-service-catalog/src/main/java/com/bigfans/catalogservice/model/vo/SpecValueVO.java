package com.bigfans.catalogservice.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author lichong
 * @create 2018-07-28 下午1:33
 **/
@Data
public class SpecValueVO implements Serializable {

    private static final long serialVersionUID = 1;

    protected Boolean selected;
    protected Boolean selectable;
    protected Boolean outOfStock;
    protected String value;
    protected String prodId;

}
