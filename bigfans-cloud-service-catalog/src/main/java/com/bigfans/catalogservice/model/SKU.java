package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.SKUEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @Title:
 * @Description: 商品SKU
 * @author lichong
 * @date 2015年9月21日 上午11:25:41
 * @version V1.0
 */
public class SKU extends SKUEntity {

	private static final long serialVersionUID = -7982565066340145527L;
	
	public static final String KEYS_SEPARATOR = ";";
	public static final String SKU_SEPARATOR = ":";

	private Integer stock;
	private Map<String , String> skuMap;
	private List<String> optIdList;
	private List<String> valIdList;

	public String getValId(String optId){
		if(skuMap == null){
			parseSkuMap();
		}
		return skuMap.get(optId);
	}

	public boolean skuMapEquals(Map<String , String> targetMap){
		if(this.skuMap == null){
			this.parseSkuMap();
		}
		boolean eq = false;
		if(targetMap.size() != this.skuMap.size()){
			return eq;
		}
		eq = true;
		for (Map.Entry<String , String> entry: this.skuMap.entrySet()) {
			if(!targetMap.get(entry.getKey()).equals(entry.getValue())){
				eq = false;
				break;
			}
		}
		return eq;
	}
	
	public void parseSkuMap(){
		if(skuMap != null){
			return ;
		}
		skuMap = new HashMap<>();
		optIdList = new ArrayList<>();
		valIdList = new ArrayList<>();
		if(skuKey == null){
			return;
		}
		String[] optValPairs = skuKey.split(KEYS_SEPARATOR);
		for(String optValPair : optValPairs){
			String[] optVal = optValPair.split(SKU_SEPARATOR);
			skuMap.put(optVal[0], optVal[1]);
			optIdList.add(optVal[0]);
			valIdList.add(optVal[1]);
		}
	}
	
	public Map<String, String> getSkuMap() {
		if(skuMap == null){
			parseSkuMap();
		}
		return skuMap;
	}
	
	public List<String> getOptIdList() {
		parseSkuMap();
		return optIdList;
	}

	public List<String> getValIdList() {
		parseSkuMap();
		return valIdList;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public boolean outOfStock(){
		return this.stock == null || this.stock == 0;
	}
}
