package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.Category;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;

public interface CategoryDAO extends BaseDAO<Category> {

	List<Category> listSub(String parentId);

	String getParentId(String catId);

	List<Category> listByLevel(Integer level);

	List<Category> listLevel1(boolean showInNav);

	List<Category> listLevel2(String parentId, boolean showInNav);

	List<Category> listLevel3(String parentId, boolean showInNav);

}
