package com.bigfans.cartservice.dao;

import com.bigfans.cartservice.model.CartItem;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;

public interface CartItemDAO extends BaseDAO<CartItem> {

	void increaseByUser(String uid, String pid, Integer amount);
	
	Long countByUser(String uid, String pid);
	
	Long sumByUser(String uid);
	
	CartItem loadByUser(String uid, String pid);
	
	List<CartItem> listByUser(String uid, Long start, Long offset);
	
	List<CartItem> getItems(String userId, boolean selected);
	
	void deleteMyItemsById(String userId, String[] ids);
	
	void deleteSelectedItems(String userId);
	
	void changeItemSelectStatus(String userId, String productId, Boolean selected);
	
	void changeAllItemsSelectStatus(String userId, Boolean selected);
	
}
