package com.bigfans.paymentservice.api;

import com.bigfans.framework.web.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * 
 * @Title: 
 * @Description: 支付
 * @author lichong 
 * @date 2016年3月1日 上午9:32:57 
 * @version V1.0
 */
@Controller
public class PaymentMockApi extends BaseController {
	
	/**
	 * 模拟第三方支付平台,直接返回成功或失败
	 * @param orderId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/pay_mock",method=RequestMethod.GET)
	public String mock(@RequestParam(name="oid") String orderId , @RequestParam(name="price") String totalPrice ) throws Exception{
		putData("oid",orderId);
		putData("price",totalPrice);
		StringBuilder params = new StringBuilder();
		params.append("?oid=").append(orderId).append("&price=").append(totalPrice);
		putData("successCallback", "http://localhost:8080/myshop/pay_success" + params);
		putData("failureCallback", "http://localhost:8080/myshop/pay_failure" + params);
		return "payment/mock";
	}
}
