package com.bigfans.searchservice.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * @Description:商品
 * @author lichong 2015年3月18日上午10:48:41
 *
 */
@Data
public class Product {

	private static final long serialVersionUID = -4961274117193266934L;

	protected String id;
	protected Date createDate;
	protected Date updateDate;
	protected Integer deleted = 0;
	// 货品ID
	protected String pgId;
	protected String brandId;
	protected String categoryId;
	protected String origin;
	// 商品名称
	protected String name;
	// 进货价格
	protected BigDecimal purchasePrice;
	// 当前价格
	protected BigDecimal price;
	// 商品重量
	protected BigDecimal weight;
	// 默认显示图片
	protected String imagePath;
	// 是否上架
	protected Boolean isOnSale;
	protected Boolean isBest;
	protected Boolean isNew;
	protected Boolean isHot;
	// 商品类别名称
	private String categoryName;
	// 商品品牌
	private String brandName;
	// 属性
	private List<ProductAttribute> attributes = new ArrayList<ProductAttribute>();

	@Override
	public String toString() {
		return "Product [origin=" + origin + ", categoryId=" + categoryId + ", categoryName=" + categoryName
				+ ", brandId=" + brandId + ", brandName=" + brandName + ", pgId=" + pgId + ", name="
				+ name + ", price=" + price + ", imagePath=" + imagePath + ", id=" + id + "]";
	}

}
