package com.bigfans.searchservice.api;

import com.bigfans.framework.utils.PinyinHelper;
import com.bigfans.framework.utils.StringHelper;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.searchservice.model.Tag;
import com.bigfans.searchservice.service.TagSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 标签搜索服务
 * @author lichong
 *
 */
@RestController
public class TagSearchApi extends BaseController {
	
	@Autowired
	private TagSearchService tagSearchService;

	@GetMapping(value = "/searchTips")
	public RestResponse searchTips(@RequestParam(name="q") String keyword){
		List<Tag> tipList = null;
		try {
			tipList = tagSearchService.searchTagByKeyword(PinyinHelper.toHanyupinyin(keyword) , 10);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RestResponse.ok(tipList);
	}
}
