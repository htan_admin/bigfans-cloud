package com.bigfans.model.dto.cart;

import lombok.Data;

/**
 * @author lichong
 * @create 2018-02-16 下午5:57
 **/
@Data
public class CartItemPromotionDto {

    private String prodId;
    private String promotionId;
    private String promotionName;
    private boolean selectable;
    private boolean selected;

}
