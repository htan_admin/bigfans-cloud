package com.bigfans.orderservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.orderservice.model.Invoice;

/**
 * 发票
 * @author lichong
 *
 */
public interface InvoiceDAO extends BaseDAO<Invoice> {

}
