package com.bigfans.orderservice.model;

import lombok.Data;

/**
 * @author lichong
 * @create 2018-02-17 下午6:00
 **/
@Data
public class OrderItemSpec {

    private String option;
    private String value;

}
