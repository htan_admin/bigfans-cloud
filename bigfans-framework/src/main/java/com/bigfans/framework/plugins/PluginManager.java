package com.bigfans.framework.plugins;

import java.util.HashMap;
import java.util.Map;

import com.bigfans.framework.Applications;

/**
 * 
 * @Title:
 * @Description: 插件管理
 * @author lichong
 * @date 2015年12月22日 上午10:07:50
 * @version V1.0
 */
@Deprecated
public class PluginManager {

	private PluginManager() {
	}

	public Plugin getPlugin(String key) {
		return pluginList.get(key);
	}

	private static Map<String, Plugin> pluginList = new HashMap<String, Plugin>();

	public static void pluginAll() {
		reload();
	}

	public static void reload() {

//		CachePlugin cachePlugin = new RedisCachePlugin();
//		cachePlugin.plugin();
//		pluginList.put("plugin_cache", cachePlugin);
//
//		FileStoragePlugin fsPlugin = new QiniuFileStoragePlugin();
//		fsPlugin.plugin();
//		pluginList.put("plugin_file_storage_qiniu", fsPlugin);
//
//		FileStoragePlugin fsLocalPlugin = new LocalStoragePlugin();
//		fsLocalPlugin.plugin();
//		pluginList.put("plugin_file_storage_local", fsLocalPlugin);
//
//		SmsPlugin smsPlugin = new AlidayuSmsPlugin();
//		smsPlugin.plugin();
//		pluginList.put("plugin_sms", smsPlugin);

	}

	public static void unpluginAll() {
		// 倒序移除插件
		Plugin[] plugins = pluginList.values().toArray(new Plugin[pluginList.size()]);
		for (int i = plugins.length - 1; i >= 0; i--) {
			plugins[i].unplugin();
		}
	}

	public static FileStoragePlugin getFileStoragePlugin() {
		String imageStoragePlugin = Applications.getSystemSetting().getImageStoragePlugin();
		return getFileStoragePlugin(imageStoragePlugin);
	}
	
	public static FileStoragePlugin getFileStoragePlugin(String storageServer) {
		FileStoragePlugin plugin = null;
		switch (storageServer) {
		case "qiniu":
				plugin = (FileStoragePlugin) pluginList.get("plugin_file_storage_qiniu");
			break;
		default:
				plugin = (FileStoragePlugin) pluginList.get("plugin_file_storage_local");
			break;
		}
		return plugin;
	}

//	public static CachePlugin getCachePlugin() {
//		return (CachePlugin) pluginList.get("plugin_cache");
//	}

	public static SmsPlugin getSmsPlugin() {
		return (SmsPlugin) pluginList.get("plugin_sms");
	}
}
